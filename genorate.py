import json

quirks = {}

quirks["cap"] = input("[U]ppercase, [L]owercase or [A]lternating case: ").lower()
if quirks["cap"] not in ["u", "l", "r", "d"]: quirks.pop("cap")

replaceNum = 0
while True:
	replaceA = input("Input text to be replaced: ")
	if replaceA == "": break
	replaceB = input("Input replacement text: ")
	quirks["replace"+str(replaceNum)] = [replaceA, replaceB]
	replaceNum += 1

fix = input("Prefix: ")
if fix != "": quirks["prefix"] = fix
fix = input("Postfix: ")
if fix != "": quirks["postfix"] = fix

randReplace = input("Random replacement text: ")
if randReplace != "": quirks["randreplace"] = randReplace

with open("quirk.json", "w") as write_file:json.dump(quirks, write_file)