Aight, so do you know the way the Homestuck trolls talk with weird quirks? Well, the genorate.py script lets you create custom quirks which will be exported in the form of json data, and quirkify.py applys the quirk data in the json to arbitrary text.
The genorator allows you to construct quirks with the folowing components:
* Replacements
    Replace any text with any other text
    They will be applied to the file in the order that you inputed them into the genorator
* Prefixes
    Place a text string at the beginning of the text
    There is no space between prefixes and the text by default
* Postfixes
    Place a text string at the end of the text
    There is no space between postfixes and the text by default
* Random word replacements
    Replace a random word in the text with a preset word
    If this is filled out, the outputted text will always have a replacement in it

Because the program doesn't know how many replacements you want to be part of the quirk (or if you want any of the other components), it will always ask. If you want to stop inputting replacements, or don't want to use any of the other things, just don't enter an input when prompted.
For the sake of example, I have included an example quirk with the repository, based on the concept of a self-aware stock ticker for some reason. Note that the replacements are case-sensitive, and as such, I recomend turning all lowercase letters to uppercase varients before replacing them with symbols or other letters.