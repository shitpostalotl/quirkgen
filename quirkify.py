import json, random

text = input("Input text: ")
quirkDataPath = input("Input location of quirk data: ")
with open(quirkDataPath, "r") as read_file: quirkData = json.load(read_file)

if "cap" in quirkData:
	if quirkData["cap"] in ["u", "a"]: text = text.upper()
	elif quirkData["cap"] == "l": text = text.lower()
	if quirkData["cap"] == "a":
		tempText = ""
		for i in range(len(text)):
			if i % 2 == 0:tempText += text[i].lower()
			else:tempText += text[i]
		text = tempText

for key, val in quirkData.items():
	if "replace" in key: text = text.replace(val[0], val[1])

if "randreplace" in quirkData:
	textAsList = text.split(" ")
	textAsList[random.randint(0, len(textAsList) - 1)] = quirkData["randreplace"]
	text = " ".join(textAsList)

if "prefix" in quirkData: text = quirkData["prefix"]+text

if "postfix" in quirkData: text = text+quirkData["postfix"]

print(text)